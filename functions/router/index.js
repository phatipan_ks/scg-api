
module.exports = (app)=>{
  const routes = [
        'scg', 
  ]
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });  
  for(let route in routes){
      app.use(`/${routes[route]}`, require(`./${routes[route]}`))
  }
}