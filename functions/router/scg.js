const express = require('express');
const router = express.Router();

router.get('/',(req, res)=>{
    let values = [];
    let counter = 0;
    let start = 3;
    let i = 0;
    while (i < 7) {
        start += counter
        counter += 2
        values.push(start)
        i++;
    }
    res.json({
        success: true,
        message: 'get values success.',
        data:{
            title: 'find values x, y, x',
            proposition : [3,5,9,'x','y','z'],
            answer : values
        }
    });
})

router.get('/place', (req, res)=> {
    const got = require('got');
 
    (async () => {
        try {
            const response = await got('https://maps.googleapis.com/maps/api/place/textsearch/json?query=restaurants+in+Bangsue&key=AIzaSyCUQW3tOWQ7KwnukeLteyMNaftAG9JjklQ');
            res.send(response.body);
        } catch (error) {
            res.json(error.response.body);
        }
    })();
})

module.exports =  router;